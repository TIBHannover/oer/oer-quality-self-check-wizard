/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-11
 * @modify date 2020-11-11
 * @desc [description]
 */



function closeWindow(){
   setTimeout (window.close, 1000);
   window.location.replace('https://www.google.com');
}



   let result = new Map();
   function queryById(id, page, step, percent) {
	  if(step !== 'undefined' && percent !== 'undefined'){
		 result.set(step, percent+"%");
	  }

   if(id!=='undefined')
		  countPage=id
	   else
	 countPage++;
		movingPagination(page);
   }


   var countPage = 1;
   
   
	function movingPagination(page) {
			if(countPage != 0 && countPage != 12){
			 $('button[id^="previous"]').css("display", "none");
			 $('button[id^="next"]').css("display", "none");
			 $('div[id^="step-"]').css("display", "none"); 
			 $('div[id^="footer"]').css("display", "none");
			 $('button[id^="send"]').css("display", "none");
			 $('button[id^="close"]').css("display", "none");
				document.getElementById("step-"+countPage).style.display = "block";		
			   document.getElementById("image").style.display = "block";
   
			if(countPage == 2 || countPage == 3 || countPage == 4 || countPage == 5 || countPage == 6 || countPage == 7 || countPage == 8 
		  || countPage == 9 || countPage == 10 || countPage == 11){
			$('div[id^="image"]').css("display", "none");
			document.getElementById("footer").style.display = "block";
			document.getElementById("previous").style.display = "block";
   
			if(countPage == 9){
			   document.getElementById("next").style.display = "block";
			}
			
			   document.getElementById("step-"+countPage).style.display = "block";	
		  }	
		  if(countPage == 9){
			document.getElementById("s2").innerHTML = result.get("step-2");
			document.getElementById("s3").innerHTML = result.get("step-3");
			document.getElementById("s4").innerHTML = result.get("step-4");
			document.getElementById("s5").innerHTML = result.get("step-5");
			document.getElementById("s6").innerHTML = result.get("step-6");
			document.getElementById("s7").innerHTML = result.get("step-7");
			document.getElementById("s8").innerHTML = result.get("step-8");
		  }
		  
		  if(countPage == 11){
			$('button[id^="previous"]').css("display", "none");
			document.getElementById("close").style.display = "block";
		   
		  }
		  
	  }
	}
   
   
   $(document).ready(function () {
	  $("#exampleModal").modal({
			  backdrop: 'static',
			  keyboard: false,
			  show: true
		  })
	  // $('.content').richText(); 
		 movingPagination();
   });
   

   function next() {
	  countPage++;
	  queryById(10,10,`step-10`,0);
	   $('div[id^="step-9"]').css("display", "none");
	  document.getElementById("step-"+countPage).style.display = "block";

	  $('button[id^="previous"]').css("display", "none");
	   $('button[id^="next"]').css("display", "none");

	   document.getElementById("send").style.display = "block";

   }


   function previous() {
	   if(countPage > 0 && countPage <=11){
		   countPage--;
			   movingPagination(); 
	   }
   }



   function send(){

   $('div[id^="step-10"]').css("display", "none");
   $('button[id^="send"]').css("display", "none");
	  queryById(11,11,`step-11`,0);
	  document.getElementById("step-11").style.display = "block";  
   }

   function closeModal(){
		 countPage = 1;
		 result.clear();
		 $('.richText-editor')[0].innerHTML='';
		
		 $('button[id^="previous"]').css("display", "none");
		 $('div[id^="step"]').css("display", "none");
		 $('button[id^="send"]').css("display", "none");

		 document.getElementById("image").style.display = "block";
		 document.getElementById("step-"+countPage).style.display = "block";
		 document.getElementById("close").style.display = "block";
   }




   function change(id){
   $(document).ready(function() {
   var object = document.querySelectorAll(".ptext6");
   
   object.forEach(function(userItem) {
   
	  userItem.style.backgroundColor = '#d4d9e9';
   
	  });
   
	  document.getElementById(id).style.backgroundColor = '#5cb85c';
   });
   }
