# OER Wizard

With our OER quality check you can independently check the quality of your educational resources and use the evaluation as a benchmark for further improvements. The individual quality dimensions are listed separately in the evaluation in order to highlight strengths and weaknesses of the educational resource in a differentiated way.


link to [Wizard quality check](https://tibhannover.gitlab.io/oer/oer-quality-self-check-wizard/html/wizard-modal.html)

## Installation

Checking out the sources

git clone `https://gitlab.com/TIBHannover/oer/oer-quality-self-check-wizard.git`



Change to the project directory

cd ~/oer-quality-self-check-wizard/wizard/html
-   open `wizard-modal.html` with browser

## Technologies
- jQuery (SmartWizard)
- bootstrap
- JavaScript
- css stylesheet
